import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
    }
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    parsed_json = json.loads(response.content)
    picture = parsed_json["photos"][0]["src"]["original"]
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return {"picture_url": picture}


def get_lat_long(location):
    """
    Returns the latitude and longitude of a location for
    specified location using OpenWeatherMap API.
    """
    # Get the latitude and longitude of the location
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    # Create the parameters for the request
    params = {
        "q": f"{location.city},{location.state.abbreviation}, USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Make the request
    response = requests.get(base_url, params=params)
    # Parse the JSON response
    parsed_json = json.loads(response.content)
    # Return the latitude and longitude
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"],
    }


def get_weather_data(location):
    """
    Returns the current weather data for a location
    using the OpenWeatherMap API.
    """
    # Get the latitude and longitude of the location
    lat_long = get_lat_long(location)
    print(lat_long)
    # Get the weather data
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    # Create the parameters for the request
    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Make the request
    response = requests.get(base_url, params=params)
    # Parse the JSON response
    parsed_json = json.loads(response.content)
    # add temp and description to a dictionary
    weather_data = {
        "temperature": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }
    # return the dictionary
    return weather_data
